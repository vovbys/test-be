package ru.ivbys.beejee.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.ivbys.beejee.data.FileHolder;
import ru.ivbys.beejee.services.FileStorageService;

import java.util.UUID;

@RestController
@RequestMapping("/files/")
public class FileStorageController {
    private final FileStorageService service;

    @Autowired
    public FileStorageController(FileStorageService service) {
        this.service = service;
    }

    @ApiOperation(value = "Get file from FileStorage")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. Return file"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),
            @ApiResponse(code = 404, message = "File not found")
    })
    @GetMapping("{uuid}")
    public ResponseEntity getFile(@PathVariable UUID uuid) {
        FileHolder fileHolder = service.getFile(uuid);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileHolder.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileHolder.toString() + "\"")
                .body(fileHolder.getData());

    }

    @ApiOperation(value = "Upload file to FileStorage")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. File uploaded"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),

    })
    @PostMapping("upload")
    public UUID upload(@RequestParam("file") MultipartFile file) {
        return service.save(file);
    }
}

