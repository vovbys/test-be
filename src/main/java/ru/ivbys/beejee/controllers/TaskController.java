package ru.ivbys.beejee.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ivbys.beejee.data.Task;
import ru.ivbys.beejee.services.TaskService;

import java.util.UUID;

@RestController
@RequestMapping("/tasks/")
public class TaskController {
    private final TaskService service;

    @Autowired
    public TaskController(TaskService service) {
        this.service = service;
    }

    @ApiOperation(value = "Get task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. Return task"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @GetMapping("{id}")
    public Task getFile(@PathVariable Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Save task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. Task saved"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),

    })
    @PostMapping("save")
    public Long upload(@RequestBody Task task) {
        return service.save(task);
    }

    @ApiOperation(value = "Save task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. Task saved"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),

    })
    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}

