package ru.ivbys.beejee.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ivbys.beejee.data.User;
import ru.ivbys.beejee.data.ViewTask;
import ru.ivbys.beejee.services.ViewService;

import java.util.List;

@RestController
@RequestMapping("/views/")
public class ViewController {
    private final ViewService service;

    @Autowired
    public ViewController(ViewService service) {
        this.service = service;
    }

    @ApiOperation(value = "Get tasks list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. List returned"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication")
    })
    @GetMapping("tasks")
    public List<ViewTask> getTasks() {
        return service.getTasks();
    }

    @ApiOperation(value = "get users list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully. List returned"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 401, message = "Error authentication"),

    })
    @GetMapping("users")
    public List<User> getUsers() {
        return service.getUsers();
    }
}

