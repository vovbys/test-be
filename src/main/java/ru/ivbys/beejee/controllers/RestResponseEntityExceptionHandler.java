package ru.ivbys.beejee.controllers;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.ivbys.beejee.exceptions.FileNotFoundException;
import ru.ivbys.beejee.exceptions.FileNotSavedException;
import ru.ivbys.beejee.exceptions.TaskNotFoundException;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = FileNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    String notFound(FileNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = FileNotSavedException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    String notCreated(FileNotSavedException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = TaskNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    String notFoundTask(FileNotSavedException ex) {
        return ex.getMessage();
    }
}
