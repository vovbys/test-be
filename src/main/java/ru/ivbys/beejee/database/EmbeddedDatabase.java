package ru.ivbys.beejee.database;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

public class EmbeddedDatabase {
    private EmbeddedPostgres postgres;

    public EmbeddedDatabase() {
        try {
            postgres = EmbeddedPostgres.builder().setPort(1521).start();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void finalize() {
        if (postgres == null) {
            return;
        }

        try {
            postgres.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}