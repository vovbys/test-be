package ru.ivbys.beejee.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@Data
@AllArgsConstructor
public class User {
   private String name;
   private UUID userId;
}
