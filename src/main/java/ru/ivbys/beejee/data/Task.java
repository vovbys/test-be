package ru.ivbys.beejee.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static javax.persistence.GenerationType.IDENTITY;


@Data

public class Task {
    private Long taskId;
    @NotNull
    private UUID userId;
    @NotNull
    private String description;
    @NotNull
    @Email
    private String mail;
}
