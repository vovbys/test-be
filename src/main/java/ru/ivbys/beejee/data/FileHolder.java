package ru.ivbys.beejee.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileHolder {
    private String name;
    private String contentType;
    private byte[] data;
}
