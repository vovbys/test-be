package ru.ivbys.beejee.data;

import lombok.Data;

import java.util.UUID;


@Data
public class ViewTask {
    private Long taskId;
    private UUID userId;
    private String description;
    private String mail;
    private String name;
}
