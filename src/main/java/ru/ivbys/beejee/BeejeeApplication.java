package ru.ivbys.beejee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.ivbys.beejee.database.EmbeddedDatabase;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableJpaRepositories(basePackages = {"ru.ivbys.beejee"})
@EnableTransactionManagement
public class BeejeeApplication {
	private static EmbeddedDatabase database =  new EmbeddedDatabase();

	public static void main(String[] args) {
		SpringApplication.run(BeejeeApplication.class, args);
	}

}
