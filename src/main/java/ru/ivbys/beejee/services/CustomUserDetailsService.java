package ru.ivbys.beejee.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.ivbys.beejee.repositories.UserRepository;
import ru.ivbys.beejee.security.CustomUserPrincipal;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) {
        return userRepository.findByName(name)
                .map(CustomUserPrincipal::new)
                .orElseThrow(()->new UsernameNotFoundException(name));
    }
}

