package ru.ivbys.beejee.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.ivbys.beejee.data.FileHolder;
import ru.ivbys.beejee.exceptions.FileNotFoundException;
import ru.ivbys.beejee.exceptions.FileNotSavedException;
import ru.ivbys.beejee.models.FileEntity;
import ru.ivbys.beejee.repositories.FileRepository;
import ru.ivbys.beejee.security.CurrentUser;

import java.io.IOException;
import java.util.UUID;

@Service
public class FileStorageService {
    private final FileRepository repository;

    @Autowired
    public FileStorageService(FileRepository repository) {
        this.repository = repository;
    }

    public UUID save(MultipartFile file) {
        return repository.save(createEntity(file)).getFileId();
    }

    public FileHolder getFile(UUID uuid) {
        return repository.findById(uuid)
                .filter(this::checkUser)
                .map(this::convert)
                .orElseThrow(FileNotFoundException::new);

    }

    private boolean checkUser(FileEntity p) {
        return p.getUserId().equals(CurrentUser.get().getUserId());
    }

    private FileHolder convert(FileEntity p) {
        return FileHolder
                .builder()
                .contentType(p.getContentType())
                .data(p.getData())
                .name(p.getName())
                .build();
    }

    private FileEntity createEntity(MultipartFile file) {
        FileEntity entity = new FileEntity();
        try {
            entity.setData(file.getBytes());
        } catch (IOException e) {
            throw new FileNotSavedException();
        }
        entity.setContentType(file.getContentType());
        entity.setName(file.getName());
        entity.setUserId(CurrentUser.get().getUserId());
        return entity;
    }
}
