package ru.ivbys.beejee.services;


import org.springframework.stereotype.Service;
import ru.ivbys.beejee.data.User;
import ru.ivbys.beejee.data.ViewTask;
import ru.ivbys.beejee.models.UserEntity;
import ru.ivbys.beejee.models.ViewTaskEntity;
import ru.ivbys.beejee.repositories.UserRepository;
import ru.ivbys.beejee.repositories.ViewTaskRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.ivbys.beejee.security.CurrentUser.get;

@Service
public class ViewService {
    private final UserRepository userRepository;
    private final ViewTaskRepository taskRepository;

    public ViewService(UserRepository userRepository,
                       ViewTaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    public List<User> getUsers() {
        List<UserEntity> all;
        if (get().isAdmin()) {
            all = userRepository.findAll();
        } else {
            all = userRepository.findByUserId(get().getUserId());
        }
        return all
                .stream()
                .map(this::convert)
                .collect(toList());
    }

    private User convert(UserEntity entity) {
        return new User(entity.getName(), entity.getUserId());
    }

    public List<ViewTask> getTasks() {
        List<ViewTaskEntity> all;
        if (get().isAdmin()) {
            all = taskRepository.findByDeleted(false);
        } else {
            all = taskRepository.findByUserIdAndAndDeleted(get().getUserId(), false);
        }
        return all
                .stream()
                .map(this::convert)
                .collect(toList());
    }

    private ViewTask convert(ViewTaskEntity entity) {
        ViewTask task = new ViewTask();
        task.setDescription(entity.getDescription());
        task.setMail(entity.getMail());
        task.setName(entity.getUserName());
        task.setTaskId(entity.getTaskId());
        task.setUserId(entity.getUserId());

        return task;
    }
}
