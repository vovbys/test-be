package ru.ivbys.beejee.services;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ivbys.beejee.data.Task;
import ru.ivbys.beejee.exceptions.TaskNotFoundException;
import ru.ivbys.beejee.models.TaskEntity;
import ru.ivbys.beejee.models.UserEntity;
import ru.ivbys.beejee.repositories.TaskRepository;
import ru.ivbys.beejee.security.CurrentUser;

import static java.util.Optional.of;

@Service
public class TaskService {
    private final TaskRepository repository;

    public TaskService(TaskRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public Task get(Long id) {
        return repository.findById(id)
                .filter(this::checkPermission)
                .map(this::convert)
                .orElseThrow(() -> new TaskNotFoundException("Not found task with id=" + id));
    }

    @Transactional
    public Long save(Task task) {
        if (task.getTaskId() != null) {
            return update(task);
        }

        return of(task)
                .filter(this::checkPermission)
                .map(this::convert)
                .map(p -> repository.save(p).getTaskId())
                .get();
    }

    private Long update(Task task) {
        return repository.findById(task.getTaskId())
                .filter(this::checkPermission)
                .map(p -> convert(task))
                .map(p -> {
                    p.setTaskId(task.getTaskId());
                    return p;
                })
                .map(p -> repository.save(p).getTaskId())
                .orElseThrow(() -> new TaskNotFoundException("Not found task with id=" + task.getTaskId()));
    }

    @Transactional
    public int delete(long taskId) {
        return repository.delete(taskId);
    }

    private TaskEntity convert(Task task) {
        TaskEntity entity = new TaskEntity();
        entity.setDescription(task.getDescription());
        entity.setMail(task.getMail());
        entity.setTaskId(task.getTaskId());
        entity.setUserId(task.getUserId());
        return entity;
    }

    private Task convert(TaskEntity entity) {
        Task task = new Task();
        task.setDescription(entity.getDescription());
        task.setMail(entity.getMail());
        task.setTaskId(entity.getTaskId());
        task.setUserId(entity.getUserId());
        return task;
    }

    private boolean checkPermission(Task task) {
        UserEntity user = CurrentUser.get();
        return user.isAdmin() || user.getUserId().equals(task.getUserId());
    }

    private boolean checkPermission(TaskEntity entity) {
        UserEntity user = CurrentUser.get();
        return user.isAdmin() || user.getUserId().equals(entity.getUserId());
    }
}
