package ru.ivbys.beejee.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FileNotSavedException extends RuntimeException {
    public FileNotSavedException(String message) {
        super(message);
    }
}
