package ru.ivbys.beejee.security;

final class RoleConstants {
    static final String ADMIN = "Admin";
    static final String USER = "UserEntity";

    private RoleConstants() {
    }
}
