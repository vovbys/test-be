package ru.ivbys.beejee.security;

import ru.ivbys.beejee.models.UserEntity;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

public class CurrentUser {
    public static UserEntity get() {
        return ((CustomUserPrincipal) getContext().getAuthentication().getPrincipal()).getUser();
    }

    private CurrentUser() {
    }
}
