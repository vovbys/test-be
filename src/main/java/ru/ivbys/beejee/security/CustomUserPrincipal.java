package ru.ivbys.beejee.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.ivbys.beejee.models.UserEntity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static ru.ivbys.beejee.security.RoleConstants.ADMIN;
import static ru.ivbys.beejee.security.RoleConstants.USER;

public class CustomUserPrincipal implements UserDetails {
    @Getter
    private final UserEntity user;

    public CustomUserPrincipal(UserEntity user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(USER));
        if (user.isAdmin()) {
            authorities.add(new SimpleGrantedAuthority(ADMIN));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
