package ru.ivbys.beejee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ivbys.beejee.models.ViewTaskEntity;

import java.util.List;
import java.util.UUID;

@Repository
public interface ViewTaskRepository extends JpaRepository<ViewTaskEntity, Long> {

    List<ViewTaskEntity> findByUserIdAndAndDeleted(UUID userId, boolean deleted);

    List<ViewTaskEntity> findByDeleted(boolean deleted);
}