package ru.ivbys.beejee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ivbys.beejee.models.TaskEntity;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
    @Modifying
    @Query(value = "update be_tasks set deleted = true where task_id = :task_id ", nativeQuery = true)
    int delete(@Param("task_id") long taskId);
}