package ru.ivbys.beejee.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.GenerationType.SEQUENCE;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "be_tasks")
@Data
public class TaskEntity extends AbstractTaskEntity {
}
