package ru.ivbys.beejee.models;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.GenerationType.SEQUENCE;

@MappedSuperclass
@Data
abstract class AbstractTaskEntity {
    private static final String SEQ = "seq_be_tasks";

    @Id
    @Column(name = "TASK_ID")
    @SequenceGenerator(name = SEQ, allocationSize = 1, sequenceName = SEQ)
    @GeneratedValue(generator = SEQ, strategy = SEQUENCE)
    private Long taskId;
    private UUID userId;
    private String description;
    private String mail;
    private UUID fileId;
    private boolean deleted;
}
