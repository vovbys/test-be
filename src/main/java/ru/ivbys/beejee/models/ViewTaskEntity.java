package ru.ivbys.beejee.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "be_view_tasks")
@Data
@EqualsAndHashCode(callSuper = true)
public class ViewTaskEntity extends AbstractTaskEntity {

    private String userName;
}