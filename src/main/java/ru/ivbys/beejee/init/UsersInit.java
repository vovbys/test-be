package ru.ivbys.beejee.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.ivbys.beejee.models.UserEntity;
import ru.ivbys.beejee.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class UsersInit {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;

    @Autowired
    public UsersInit(PasswordEncoder passwordEncoder,
                     UserRepository repository) {
        this.passwordEncoder = passwordEncoder;
        this.repository = repository;
    }

    void init() {
        repository.saveAll(getUsers());
    }

    private List<UserEntity> getUsers() {
        List<UserEntity> users = new ArrayList<>(2);
        UserEntity user = new UserEntity();
        user.setAdmin(true);
        user.setName("admin");
        user.setPassword(passwordEncoder.encode("123"));

        users.add(user);

        user = new UserEntity();
        user.setAdmin(false);
        user.setName("user");
        user.setPassword(passwordEncoder.encode("123"));

        users.add(user);

        return users;
    }
}
