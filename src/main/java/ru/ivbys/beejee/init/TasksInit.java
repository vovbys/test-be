package ru.ivbys.beejee.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.ivbys.beejee.models.FileEntity;
import ru.ivbys.beejee.models.TaskEntity;
import ru.ivbys.beejee.repositories.FileRepository;
import ru.ivbys.beejee.repositories.TaskRepository;
import ru.ivbys.beejee.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class TasksInit {
    private final TaskRepository repository;
    private final UserRepository userRepository;
    private final FileRepository fileRepository;

    @Autowired
    public TasksInit(TaskRepository repository,
                     UserRepository userRepository,
                     FileRepository fileRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.fileRepository = fileRepository;
    }

    void init() {
        repository.saveAll(getTasks());
    }

    private List<TaskEntity> getTasks() {
        UUID admin = getUserId("admin");
        UUID user = getUserId("user");

        List<TaskEntity> tasks = new ArrayList<>(2);

        TaskEntity task = new TaskEntity();
        task.setDescription("task for admin");
        task.setMail("admin@ivbys.com");
        task.setUserId(admin);
        task.setFileId(getFileId(admin));

        tasks.add(task);

        task = new TaskEntity();
        task.setDescription("task for user");
        task.setMail("user@ivbys.com");
        task.setUserId(user);
        task.setFileId(getFileId(user));

        tasks.add(task);

        return tasks;
    }

    private UUID getFileId(UUID user) {
        return fileRepository
                .findAll()
                .stream()
                .filter(p->p.getUserId()
                        .equals(user))
                .findAny()
                .map(FileEntity::getFileId)
                .orElse(null);
    }

    private UUID getUserId(String admin) {
        return userRepository
                .findByName(admin)
                .orElseThrow(() -> new UsernameNotFoundException(admin))
                .getUserId();
    }
}
