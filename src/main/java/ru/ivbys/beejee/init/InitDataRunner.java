package ru.ivbys.beejee.init;


import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitDataRunner implements CommandLineRunner {
    private final UsersInit usersInit;
    private final TasksInit tasksInit;
    private final FilesInit filesInit;

    public InitDataRunner(UsersInit usersInit,
                          TasksInit tasksInit,
                          FilesInit filesInit) {
        this.usersInit = usersInit;
        this.tasksInit = tasksInit;
        this.filesInit = filesInit;
    }

    @Override
    public void run(String... args) {
        usersInit.init();
        filesInit.init();
        tasksInit.init();
    }
}
