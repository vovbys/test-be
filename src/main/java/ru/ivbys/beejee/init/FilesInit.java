package ru.ivbys.beejee.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.ivbys.beejee.models.FileEntity;
import ru.ivbys.beejee.repositories.FileRepository;
import ru.ivbys.beejee.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@Component
public class FilesInit {
    private final FileRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public FilesInit(FileRepository repository,
                     UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    void init() {
        repository.saveAll(getFiles());
    }

    private List<FileEntity> getFiles() {
        UUID admin = getUserId("admin");
        UUID user = getUserId("user");
        byte[] emptyData = new byte[0];

        List<FileEntity> files = new ArrayList<>(2);
        FileEntity file = new FileEntity();

        file.setContentType(TEXT_PLAIN_VALUE);
        file.setName("test_file_admin.txt");
        file.setUserId(admin);
        file.setData(emptyData);

        files.add(file);

        file = new FileEntity();

        file.setContentType(TEXT_PLAIN_VALUE);
        file.setName("test_file_user.txt");
        file.setUserId(user);
        file.setData(emptyData);

        files.add(file);

        return files;
    }

    private UUID getUserId(String name) {
        return userRepository
                .findByName(name)
                .orElseThrow(() -> new UsernameNotFoundException(name))
                .getUserId();
    }
}
